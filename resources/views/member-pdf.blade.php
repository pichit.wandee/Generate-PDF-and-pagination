<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PDF-Pagination demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  </head>
  <body>
   
    <div class="container">
        <div class="row">
            <div class="col-lg-12" style="margin-top: 15px">
                <div class="pull-left">
                    <h2>Generate PDF Using DomPDF</h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{route('member-pdf',['download'=>'pdf'])}}">Download PDF</a>
                </div>
            </div>
        </div>
        <br>
        <table class="table table-bordered">
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Address</th>
                <th>Country</th>
            </tr>
         @foreach ($member as $members)
            <tr>
                <td>{{ $members->name}}</td>
                <td>{{ $members->email}}</td>
                <td>{{ $members->address}}</td>
                <td>{{ $members->country}}</td>
            </tr>
         @endforeach
        </table>
        {{$member->links('pagination::bootstrap-4')}}
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>
  </body>
</html>